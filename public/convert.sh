#!/bin/bash -ex

convert() {
    file=$1
    rate=$2
    ffmpeg -i "$file" -map 0:a -codec:a libopus -b:a $rate -vbr on "${file//\.mp3}.caf" -y &
    ffmpeg -i "$file" -map 0:a -codec:a libopus -b:a $rate -vbr on "${file//\.mp3}.ogg" -y &
}


convert "01 Mitch Altman Interview.mp3" 75k
convert "02 Elishka Pirova Interview.mp3" 45k
wait
convert "03 Simon Repp Interview.mp3" 40k
convert "04 Estelle Masse Interview.mp3" 40k
wait
convert "05 Sascha Meinrath Interview.mp3" 24k
convert "06 Dirk Jäckel Interview.mp3" 85k
wait
